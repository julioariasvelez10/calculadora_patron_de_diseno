public class CalculatorController
{
    private CalculatorModel model;
    private CalculatorView view;

    public CalculatorController(CalculatorModel model, CalculatorView view)
    {
        this.model = model;
        this.view = view;
    }

    public void Run()
    {
        while (true)
        {
            Console.WriteLine("Enter the first number:");
            double num1 = view.GetNumberInput();

            Console.WriteLine("Enter the second number:");
            double num2 = view.GetNumberInput();

            Console.WriteLine("Enter the operation (+, -, *, /):");
            char operation = Console.ReadKey().KeyChar;
            Console.WriteLine();

            double result = 0;

            switch (operation)
            {
                case '+':
                    result = model.Add(num1, num2);
                    break;
                case '-':
                    result = model.Subtract(num1, num2);
                    break;
                case '*':
                    result = model.Multiply(num1, num2);
                    break;
                case '/':
                    result = model.Divide(num1, num2);
                    break;
                default:
                    Console.WriteLine("Invalid operation.");
                    continue;
            }

            view.DisplayResult(result);
        }
    }
}

//Diseñé el Controlador (CalculatorController):
//Definí las dependencias del Modelo y la Vista en el Controlador.
//Creé un constructor para el Controlador que tome el Modelo y la Vista como parámetros y los almacene en variables de instancia.
//Implementé un método Run () en el Controlador para orquestar el flujo de la aplicación.
//Dentro del método Run (), obtuve la entrada del usuario utilizando la Vista y realicé las operaciones correspondientes utilizando el Modelo.
//Actualicé la Vista con el resultado utilizando el método DisplayResult() de la Vista.
