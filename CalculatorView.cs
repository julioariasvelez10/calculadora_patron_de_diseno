public class CalculatorView
{
    public double GetNumberInput()
    {
        double number;
        while (!double.TryParse(Console.ReadLine(), out number))
        {
            Console.WriteLine("Invalid input. Please enter a valid number.");
        }
        return number;
    }

    public void DisplayResult(double result)
    {
        Console.WriteLine("Result: " + result);
    }
}
// Creé la Vista (CalculatorView):
//Determine qué interacciones se necesitan con el usuario, como ingresar números y mostrar resultados.
//Implementé métodos en la Vista para obtener la entrada del usuario y mostrar resultados en la consola.
