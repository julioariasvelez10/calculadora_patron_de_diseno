public class CalculatorModel
{
    public double Add(double a, double b)
    {
        return a + b;
    }

    public double Subtract(double a, double b)
    {
        return a - b;
    }

    public double Multiply(double a, double b)
    {
        return a * b;
    }

    public double Divide(double a, double b)
    {
        if (b != 0)
            return a / b;
        else
            throw new DivideByZeroException("Cannot divide by zero.");
    }

    // Otras operaciones matemáticas necesarias para la calculadora científica
}

//Definí la funcionalidad del Modelo (CalculatorModel):
//Identifiqué las operaciones matemáticas necesarias para una calculadora científica, como suma, resta, multiplicación y división.
//Implementé métodos en el Modelo para cada una de estas operaciones.
