﻿internal class Program
{
    private static void Main(string[] args)
     {
        CalculatorModel model = new CalculatorModel();
        CalculatorView view = new CalculatorView();
        CalculatorController controller = new CalculatorController(model, view);

        controller.Run();
    }
}

//En el programa principal (Main()):
//Instancié el Modelo (CalculatorModel), la Vista (CalculatorView) y el Controlador (CalculatorController).
//Pasé el Modelo y la Vista al Controlador a través de su constructor.
//Llamé al método Run() del Controlador para iniciar la aplicación de la calculadora científica.
